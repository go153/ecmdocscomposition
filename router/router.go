package router

import (
	"strings"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/ecmdocscomposition/composition"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/configuration"
)

func StartService() {
	c := configuration.FromFile()

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: c.GetAllowedDomains(),
	}))

	r := e.Group("/api/v1/generatetemplate", user.ValidateJWTInternal)

	r.POST("", composition.GenerateTemplate)
	r.POST("/procedure", composition.GenerateTemplateProcedure)
	// Rutas para validar estado del servicio
	healthRoute(e)

	// Inicia el servidor
	if strings.ToLower(c.AppIsSSL) == "t" {
		e.Logger.Fatal(e.StartTLS(":"+c.AppPort, c.AppCertificate, c.AppPrivateKey))
	} else {
		e.Logger.Fatal(e.Start(":" + c.AppPort))
	}
}
