package composition

import (
	"bytes"
	"database/sql"
	"fmt"

	"gitlab.com/e-capture/ecatch-ecm/majosystem/db"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

type sqlserver struct{}

const (
	sqlserverExecuteSPArus = `EXECUTE [dbo].[DocComposition_GetvalueResponse] @documentid=%d `
	sqlserverExecuteSP     = `execute`
)

func (s *sqlserver) executeSPArus(dID int64) (*AdditionalValue, error) {
	conn := db.GetConnection()
	av := AdditionalValue{}
	sqlExecute := fmt.Sprintf(sqlserverExecuteSPArus, dID)
	stmt, err := conn.Prepare(sqlExecute)
	if err != nil {
		logger_trace.Error.Printf("preparando la sentencia ExecuteSP: %v", err)
		return nil, err
	}
	defer stmt.Close()
	err = stmt.QueryRow().Scan(&av.Url, &av.SignatoryID)
	if err != nil {
		logger_trace.Error.Printf("***ejecutando la sentencia ExecuteSP: %v", err)
		return nil, err
	}
	return &av, nil
}

func (s sqlserver) ExecuteSP(m *Model) ([]map[string]interface{}, error) {
	rs := make([]map[string]interface{}, 0)
	conn := db.GetConnection()
	r := bytes.Buffer{}
	r.WriteString(fmt.Sprintf(`%s %s `, sqlserverExecuteSP, m.Procedure))
	vs := make([]interface{}, 0)
	for i, v := range m.Parameters {
		if len(v) > 0 {
			_, err := r.WriteString(fmt.Sprintf(`'%s',`, v))
			if err != nil {
				logger_trace.Error.Printf("agregando parametros a la ejecucion del SP en sqlserverExecuteSP: %v", err)
				return rs, err
			}
			vs = append(vs, sql.Named(fmt.Sprintf("%s", i), v))
		}
	}

	r.Truncate(r.Len() - 1)
	fmt.Println(r.String())
	stmt, err := conn.Prepare(r.String())
	if err != nil {
		logger_trace.Error.Printf("preparando consulta sqlserverExecuteSP: %v", err)
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		logger_trace.Error.Printf("ejecutando sqlserverExecuteSP user: %v", err)
		return rs, err
	}
	defer rows.Close()

	cols, _ := rows.Columns()
	for rows.Next() {
		r := make(map[string]interface{})
		// Create a slice of interface{}'s to represent each column,
		// and a second slice to contain pointers to each item in the columns slice.
		columns := make([]interface{}, len(cols))
		columnPointers := make([]interface{}, len(cols))
		for i := range columns {
			columnPointers[i] = &columns[i]
		}
		// Scan the result into the column pointers...
		if err = rows.Scan(columnPointers...); err != nil {
			logger_trace.Error.Printf("no se pudo escanear las columnas de la consulta sqlserverGetInfoktg: %v", err)
			return nil, err
		}

		// Create our map, and retrieve the value for each column from the pointers slice,
		// storing it in the map with the name of the column as the key.
		for i, colName := range cols {
			val := columnPointers[i].(*interface{})
			r[colName] = *val
		}
		rs = append(rs, r)
	}
	return rs, nil
}
