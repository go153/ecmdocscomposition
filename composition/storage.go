package composition

import (
	"strings"

	// Esta importación se debe modificar a los paquetes del proyecto
	"gitlab.com/e-capture/ecatch-ecm/majosystem/configuration"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
)

var s Storage

func init() {
	setStorage()
}

type Storage interface {
	executeSPArus(dID int64) (*AdditionalValue, error)
	ExecuteSP(m *Model) ([]map[string]interface{}, error)
}

func setStorage() {
	c := configuration.FromFile()
	switch strings.ToLower(c.DBConnection) {
	case "sqlserver":
		s = &sqlserver{}
	default:
		logger_trace.Error.Printf("este motor de bd no está configurado aún: %s", c.DBConnection)
	}
}
