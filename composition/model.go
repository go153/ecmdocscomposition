package composition

import "gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"

type Model struct {
	DoctypeID  int               `json:"doctype_id"`
	DocumentDI int64             `json:"document_id"`
	Path       string            `json:"path"`
	Procedure  string            `json:procedure`
	Parameters map[string]string `json:"parameters"`
}

type Importf struct {
	Id            int64  `json:"id"`
	Name          string `json:"name"`
	Sequences     int    `json:"sequences"`
	DocType_Id    int64  `json:"doctype_id"`
	Keyword_Id    int64  `json:"keyword_id"`
	FieldTypes_Id int64  `json:"fieldtypes_id"`
}

type responseDC struct {
	Error bool `json:"error"`
	Data  Data `json:"data"`
	Code  int  `json:"code"`
}

type Data struct {
	ID          int64   `json:"id"`
	VersionID   int64   `json:"version_id"`
	WFexecuted  bool    `json:"wf_executed"`
	Url         *string `json:"url,omitempty"`
	SignatoryID *string `json:"signatory_id,omitempty"`
}

type AdditionalValue struct {
	Url         string `json:"url"`
	SignatoryID string `json:"signatory_id"`
}

type LoginResponse struct {
	Error bool                   `json:"error"`
	Data  map[string]interface{} `json:"data"`
	Code  int                    `json:"code"`
}

type TockenDecode struct {
	User      user.Model `json:"user"`
	IpAddress string     `json:"ip_address"`
	Exp       int64      `json:"exp"`
	Iss       string     `json:"iss"`
}

func (m *Model) ExecuteSPArus(dID int64) (*AdditionalValue, error) {
	return s.executeSPArus(dID)
}
func (m *Model) Execute() ([]map[string]interface{}, error) {
	return s.ExecuteSP(m)
}
