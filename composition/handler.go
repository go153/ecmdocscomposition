package composition

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/google/uuid"

	"gitlab.com/e-capture/ecatch-ecm/ecmauthentication/user"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/configuration"

	"github.com/SebastiaanKlippert/go-wkhtmltopdf"

	"github.com/labstack/echo"
	"gitlab.com/e-capture/ecatch-ecm/ecmstoredocument/doctypes"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/logger_trace"
	"gitlab.com/e-capture/ecatch-ecm/majosystem/response"
)

func GenerateTemplate(c echo.Context) error {

	uID := c.Get("user").(user.Model).ID

	mr := response.Model{}
	var templateID int64
	var strtemplateID string
	var kwstorage []string

	data := map[string]string{}
	err := c.Bind(&data)
	if err != nil {
		logger_trace.Error.Printf("la estructura no es válida: %v", err)
		mr.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, mr)
	}

	for i, k := range data {
		if strings.ToUpper(i) == "TEMPLATE" {
			strtemplateID = k
			templateID, err = strconv.ParseInt(k, 10, 64)
			if err != nil {
				logger_trace.Error.Printf("el template debe ser un entero: %v", err)
				mr.Set(true, nil, 107)
				return c.JSON(http.StatusAccepted, mr)
			}
			break
		}
	}

	dtyp := doctypes.Model{}

	result, err := dtyp.GetByID(templateID)
	if err != nil {
		logger_trace.Error.Printf("error no se pudo obtener el tipo documental del template: %v", err)
		mr.Set(true, nil, 108)
		return c.JSON(http.StatusAccepted, mr)
	}

	tpl, err := template.ParseFiles(result.UrlPath)
	if err != nil {
		logger_trace.Error.Printf("error en el parceo del template: %v", err)
		mr.Set(true, nil, 109)
		return c.JSON(http.StatusAccepted, mr)
	}

	t := uuid.New().String()

	path := fmt.Sprintf("./templates/tmpl-%s.html", t)

	// TODO Crear funcion que cree la carptea automaticamente
	if _, err := os.Stat(filepath.Dir("./templates/")); os.IsNotExist(err) {
		if err = os.MkdirAll(filepath.Dir("./templates/"), 0777); err != nil {
			logger_trace.Error.Printf("no se pude crear carpeta para almacenar template en pdf: %v", err)
			mr.Set(true, nil, 111)
			return c.JSON(http.StatusAccepted, mr)
		}
	}
	f, err := os.OpenFile(path, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	if err != nil {
		logger_trace.Error.Printf("error creando el archivo: %v", err)
		mr.Set(true, nil, 111)
		return c.JSON(http.StatusAccepted, mr)
	}

	defer f.Close()

	err = tpl.ExecuteTemplate(f, result.UrlPath, data)
	if err != nil {
		logger_trace.Error.Printf("error creando el archivo: %v", err)
		mr.Set(true, nil, 111)
		return c.JSON(http.StatusAccepted, mr)
	}

	err = generatePDF(path)
	if err != nil {
		logger_trace.Error.Printf("error en la generacion del PDF: %v", err)
		mr.Set(true, nil, 112)
		return c.JSON(http.StatusAccepted, mr)
	}

	var kwsjson, pathPdf string
	for j, s := range data {
		if j == "str_kws" {
			kwstorage = strings.Split(s, ",")
		}
	}
	kwsjson = "["
	//fmt.Println(data)
	for k, v := range data {
		for _, u := range kwstorage {
			if k[0:2] == "kw" {
				if strings.Replace(k, "kw", "", -1) == u {
					kwsjson = kwsjson + fmt.Sprintf(`{"id":%s,"name":"name","value":"%s"},`, strings.Replace(k, "kw", "", -1), v)
					break
				}
			}

		}
	}
	kwsjson = kwsjson[0:len(kwsjson)-1] + "]"

	token, err := login()
	if err != nil {
		logger_trace.Error.Printf("No se pudo autenticar el servicio de creacion para document en pdf: %v", err)
		mr.Set(true, nil, 2)
		return c.JSON(http.StatusAccepted, mr)
	}
	pathPdf = strings.Replace(path, ".html", ".pdf", -1)

	uISstr := fmt.Sprintf("%d", uID)

	res, err := importFile(pathPdf, strtemplateID, "1", uISstr, kwsjson, token)
	if err != nil {
		logger_trace.Error.Printf("error creando el documento: %v", err)
		mr.Set(true, nil, 44)
		return c.JSON(http.StatusAccepted, mr)
	}

	// llamado SP

	r := Model{}
	logger_trace.Info.Printf("1. Document_id:")
	logger_trace.Info.Printf(res.VersionID)

	additionalValues, err := r.ExecuteSPArus(res.ID)
	if err != nil {
		logger_trace.Error.Printf("error consultando el GetValueResponse: %v", err)
	}

	if additionalValues != nil {
		logger_trace.Info.Printf("4. additionalValues:")
		logger_trace.Info.Printf(additionalValues)
		//asigna valor
		if additionalValues.Url != "0" {
			res.Url = &additionalValues.Url
			res.SignatoryID = &additionalValues.SignatoryID
		} else {
			res.Url = nil
			res.SignatoryID = nil
		}
	}
	mr.Set(false, res, 29)
	return c.JSON(http.StatusCreated, mr)

}

func generatePDF(file string) error {
	// Create new PDF generator
	pdfg, err := wkhtmltopdf.NewPDFGenerator()
	if err != nil {
		return err
	}

	htmlfile, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}

	pdfg.AddPage(wkhtmltopdf.NewPageReader(bytes.NewReader(htmlfile)))
	pdfg.Dpi.Set(600)
	pdfg.PageSize.Set("Legal")

	// Create PDF document in internal buffer
	err = pdfg.Create()
	if err != nil {
		return err
	}

	// Write buffer contents to file on disk
	pathPdf := strings.Replace(file, ".html", ".pdf", -1)
	err = pdfg.WriteFile(pathPdf)
	if err != nil {
		return err
	}

	return nil
}

func importFile(fpath, doctype, status, user, kwsjson, token string) (*Data, error) {

	c := configuration.FromFile()
	// Prepare a form that you will submit to that URL.
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	// Add your image file
	f, err := os.Open(fpath)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	fw, err := w.CreateFormFile("file", fpath)
	if err != nil {
		return nil, err
	}
	if _, err = io.Copy(fw, f); err != nil {
		return nil, err
	}
	// Doctype
	if fw, err = w.CreateFormField("doctype"); err != nil {
		return nil, err
	}
	if _, err = fw.Write([]byte(doctype)); err != nil {
		return nil, err
	}
	// status
	if fw, err = w.CreateFormField("status"); err != nil {
		return nil, err
	}
	if _, err = fw.Write([]byte(status)); err != nil {
		return nil, err
	}
	// User
	if fw, err = w.CreateFormField("User"); err != nil {
		return nil, err
	}
	if _, err = fw.Write([]byte(user)); err != nil {
		return nil, err
	}
	// Keywords
	if fw, err = w.CreateFormField("keywords"); err != nil {
		return nil, err
	}
	if _, err = fw.Write([]byte(kwsjson)); err != nil {
		return nil, err
	}
	// Don't forget to close the multipart writer.
	// If you don't close it, your request will be missing the terminating boundary.
	w.Close()
	// Now that you have a form, you can submit it to your handler.
	req, err := http.NewRequest("POST", c.AppSTOREURL, &b)
	if err != nil {
		return nil, err
	}
	// Don't forget to set the content type, this will contain the boundary.
	req.Header.Set("Authorization", token)
	req.Header.Set("Content-Type", w.FormDataContentType())
	// Submit the request
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusCreated {
		return nil, errors.New(res.Status)
	}
	defer res.Body.Close()
	// Se crea bytes
	bi, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	// Se crea arreglos de structure ImportConf en configs
	//dat := response.Model{}
	rdc := responseDC{}

	// Se decodifica el byte en el arreglo de ImportConf data
	err = json.Unmarshal(bi, &rdc)
	if err != nil {
		return nil, err
	}
	return &rdc.Data, nil
}

func login() (string, error) {

	var tocken string
	data := LoginResponse{}
	c := configuration.FromFile()
	//se define url
	url := c.AppAUTHURL
	// Se definen parametros
	jsonStr := []byte(fmt.Sprintf(`{"email": "%s", "password": "%s"}`, c.AppUSER, c.AppPASSWORD))
	// Se arma peticion

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if err != nil {
		logger_trace.Error.Printf("Error armando peticion authentication en Timer: %v", err)
		return tocken, err
	}
	//Se envia header
	req.Header.Set("Content-Type", "application/json")
	// Se envia peticion

	client := &http.Client{}
	// Se reciben  parametros del llamado
	resp, err := client.Do(req)
	if err != nil {
		logger_trace.Error.Printf("Error enviando peticion authentication en Timer: %v", err)
		return tocken, err
	}

	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger_trace.Error.Printf("Error obteniendo respuesta de peticion authentication en Timer: %v", err)
		return tocken, err
	}
	err = json.Unmarshal(b, &data)
	if err != nil {
		logger_trace.Error.Printf("Error decodificando respuesta de peticion authentication en Timer: %v", err)
		return tocken, err
	}
	if data.Code != 30 {
		err = fmt.Errorf("Error de Conexion")
		logger_trace.Error.Printf("Error Valide Usuario y/o Password para authentication en Timer: %v", err)
		return tocken, err

	}
	for i, v := range data.Data {
		if i == "token" {
			tocken = "Bearer " + v.(string)
		}
	}

	return tocken, nil
}

func GenerateTemplateProcedure(c echo.Context) error {

	uID := c.Get("user").(user.Model).ID
	m := Model{}
	mr := response.Model{}
	var templateID int64
	var strtemplateID string
	var kwstorage []string
	data := map[string]string{}
	dataResponse := map[string]string{}
	err := c.Bind(&dataResponse)
	if err != nil {
		logger_trace.Error.Printf("la estructura no es válida: %v", err)
		mr.Set(true, nil, 1)
		return c.JSON(http.StatusAccepted, mr)
	}

	for i, k := range dataResponse {
		if strings.ToUpper(i) == "TEMPLATE" {
			strtemplateID = k
			templateID, err = strconv.ParseInt(k, 10, 64)
			if err != nil {
				logger_trace.Error.Printf("el template debe ser un entero: %v", err)
				mr.Set(true, nil, 107)
				return c.JSON(http.StatusAccepted, mr)
			}
		}
		if strings.ToLower(i) == "procedure" {
			m.Procedure = k

		}
		fmt.Println(strings.ToLower(i))
		if strings.ToLower(i) == "document_id" {
			var param = make(map[string]string, 0)
			param["document_id"] = k
			m.Parameters = param

		}
		if strings.ToLower(i) == "str_kws" {
			data["str_kws"] = k
		}
	}

	dtyp := doctypes.Model{}

	result, err := dtyp.GetByID(templateID)
	if err != nil {
		logger_trace.Error.Printf("error no se pudo obtener el tipo documental del template: %v", err)
		mr.Set(true, nil, 108)
		return c.JSON(http.StatusAccepted, mr)
	}

	//Procedure 
	if m.Procedure == "" {
		m.Procedure = "ecatch_generate_template"
	}
	
	if m.Parameters != nil {
		iData, err := m.Execute()
		if err != nil {
			logger_trace.Trace.Printf("no se pudo consular procedimiento", err)
		}
		for _, r := range iData {
			var key, value, registerData string
			for k, v := range r {
				registerData = v.(string)
				if strings.ToUpper(k) == "KEYWORD" || strings.ToUpper(k) == "STR_KWS" {
					key = registerData
				} else {
					value = registerData
				}
			}
			data[key] = value
		}
	}

	tpl, err := template.ParseFiles(result.UrlPath)
	if err != nil {
		logger_trace.Error.Printf("error en el parceo del template: %v", err)
		mr.Set(true, nil, 109)
		return c.JSON(http.StatusAccepted, mr)
	}

	t := uuid.New()

	path := fmt.Sprintf("./templates/tmpl-%s.html", t)

	// TODO Crear funcion que cree la carptea automaticamente
	if _, err := os.Stat(filepath.Dir("./templates/")); os.IsNotExist(err) {
		if err = os.MkdirAll(filepath.Dir("./templates/"), 0777); err != nil {
			logger_trace.Error.Printf("no se pude crear carpeta para almacenar template en pdf: %v", err)
			mr.Set(true, nil, 111)
			return c.JSON(http.StatusAccepted, mr)
		}
	}
	f, err := os.OpenFile(path, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	if err != nil {
		logger_trace.Error.Printf("error creando el archivo: %v", err)
		mr.Set(true, nil, 111)
		return c.JSON(http.StatusAccepted, mr)
	}

	defer f.Close()

	fmt.Println("***************************************************")
	fmt.Println("INFO TEMPLATE:", data)
	fmt.Println("***************************************************")
	err = tpl.ExecuteTemplate(f, result.UrlPath, data)
	if err != nil {
		logger_trace.Error.Printf("error creando el archivo: %v", err)
		mr.Set(true, nil, 111)
		return c.JSON(http.StatusAccepted, mr)
	}

	err = generatePDF(path)
	if err != nil {
		logger_trace.Error.Printf("error en la generacion del PDF: %v", err)
		mr.Set(true, nil, 112)
		return c.JSON(http.StatusAccepted, mr)
	}

	var kwsjson, pathPdf string
	for j, s := range data {
		if j == "str_kws" {
			kwstorage = strings.Split(s, ",")
		}
	}
	kwsjson = "["
	//fmt.Println(data)
	for k, v := range data {
		for _, u := range kwstorage {
			if k[0:2] == "kw" {
				if strings.Replace(k, "kw", "", -1) == u {
					kwsjson = kwsjson + fmt.Sprintf(`{"id":%s,"name":"name","value":"%s"},`, strings.Replace(k, "kw", "", -1), v)
					break
				}
			}

		}
	}
	kwsjson = kwsjson[0:len(kwsjson)-1] + "]"

	token, err := login()
	if err != nil {
		logger_trace.Error.Printf("No se pudo autenticar el servicio de creacion para document en pdf: %v", err)
		mr.Set(true, nil, 2)
		return c.JSON(http.StatusAccepted, mr)
	}
	pathPdf = strings.Replace(path, ".html", ".pdf", -1)

	uISstr := fmt.Sprintf("%d", uID)

	res, err := importFile(pathPdf, strtemplateID, "1", uISstr, kwsjson, token)
	if err != nil {
		logger_trace.Error.Printf("error creando el documento: %v", err)
		mr.Set(true, nil, 44)
		return c.JSON(http.StatusAccepted, mr)
	}

	mr.Set(false, res, 29)
	return c.JSON(http.StatusCreated, mr)
}
