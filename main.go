package main

import (
	"fmt"
	"time"

	"gitlab.com/e-capture/ecatch-ecm/ecmdocscomposition/router"
)

func main() {

	Mensaje("ECMDoccomposition", "Main", "V1.0.14")

	router.StartService()
}

func Mensaje(Package, line, Value string) {
	var Message string
	Message = "[" + time.Now().Format("2006-01-02-150405") + "]" + " - Paquete: " + Package + " - Linea: " + line + " - Mensaje: " + Value
	fmt.Println(Message)
}
