module gitlab.com/e-capture/ecatch-ecm/ecmdocscomposition

go 1.13

require (
	github.com/SebastiaanKlippert/go-wkhtmltopdf v1.5.0
	github.com/google/uuid v1.1.2
	github.com/labstack/echo v3.3.10+incompatible
	github.com/satori/go.uuid v1.2.0
	github.com/satori/uuid v1.2.0
	gitlab.com/e-capture/ecatch-ecm/ecmauthentication v0.0.0
	gitlab.com/e-capture/ecatch-ecm/ecmconfiguration v0.0.0
	gitlab.com/e-capture/ecatch-ecm/ecmforms v0.0.0
	gitlab.com/e-capture/ecatch-ecm/ecmstoredocument v0.0.0
	gitlab.com/e-capture/ecatch-ecm/majosystem v0.0.0
)

replace (
	gitlab.com/e-capture/ecatch-ecm/ecmauthentication => ../ecmauthentication
	gitlab.com/e-capture/ecatch-ecm/ecmconfiguration => ../ecmconfiguration
	gitlab.com/e-capture/ecatch-ecm/ecmforms => ../ecmforms
	gitlab.com/e-capture/ecatch-ecm/ecmstoredocument => ../ecmstoredocument
	gitlab.com/e-capture/ecatch-ecm/majosystem => ../majosystem/v2
)
